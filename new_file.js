function addPassion() {
    var grid = document.getElementById('passionsGrid');

    var newPassion = document.createElement('div');
    newPassion.className = 'passion-card';
    newPassion.innerHTML = `
        <img src="https://via.placeholder.com/1080" alt="New Passion" class="w-full h-64 object-cover mb-6">
        <h3 class="text-2xl font-bold text-gray-800 mb-4">New Passion</h3>
        <p class="text-gray-600">This is a new passion.</p>
    `;

    grid.appendChild(newPassion);
}